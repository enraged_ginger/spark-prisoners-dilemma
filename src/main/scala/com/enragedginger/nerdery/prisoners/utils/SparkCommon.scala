package com.enragedginger.nerdery.prisoners.utils

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

object SparkCommon {

  lazy val conf = {
    new SparkConf(false)
      .setMaster("local[*]")
      .setAppName("GraphX Example")
  }

  lazy val sparkContext = new SparkContext(conf)
  lazy val sparkSqlContext = SQLContext.getOrCreate(sparkContext)

}
