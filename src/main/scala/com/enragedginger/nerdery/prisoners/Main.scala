package com.enragedginger.nerdery.prisoners

import com.enragedginger.nerdery.prisoners.utils.SparkCommon

import scala.concurrent.Future

/**
  * @author Stephen Hopper.
  */
object Main {

  val sqlContext = SparkCommon.sparkSqlContext
  import sqlContext.implicits._

  def main(args: Array[String]) {
    val SCHEMA_OPTIONS = Map("header" -> "true", "inferSchema" -> "true")
    val path = "src/main/resources/prisoners_dilemma_tournament.csv"
    val df = sqlContext.read.format("csv").options(SCHEMA_OPTIONS).load(path)

    df.registerTempTable("source")
    val dfShort = sqlContext.sql(
      """(select prisoner_1 as prisoner, prisoner_1_time_ms as prisoner_time_ms, prisoner_1_response as prisoner_response
        |from source)
        |union all
        |(select prisoner_2 as prisoner, prisoner_2_time_ms as prisoner_time_ms, prisoner_2_response as prisoner_response
        |from source)""".stripMargin)
    dfShort.registerTempTable("prisoner_summary")

    val averagePrisonerRuntimes = dfShort.groupBy("prisoner").avg("prisoner_time_ms")
    val lowestAveragePrisonerRuntime = averagePrisonerRuntimes.orderBy($"avg(prisoner_time_ms)").first()
    val highestAveragePrisonerRuntime = averagePrisonerRuntimes.orderBy($"avg(prisoner_time_ms)".desc).first()
    println(s"a) Lowest average prisoner runtime: $lowestAveragePrisonerRuntime")
    println(s"b) Highest average prisoner runtime: $highestAveragePrisonerRuntime")

    val dfMatchupSummary = sqlContext.sql(
      """select prisoner_1, prisoner_2, sum(match_duration_s) as runtime
        |from source
        |group by prisoner_1, prisoner_2
        |order by runtime desc
      """.stripMargin
    )
    println(s"c) Slowest matchup runtime: ${dfMatchupSummary.first()}")

    val dfBalance = sqlContext.sql(
      """select prisoner, abs(0.5 - (sum(IF(prisoner_response="Confess", 1, 0)) / count(*))) as thing
        |from prisoner_summary
        |group by prisoner
        |order by thing
      """.stripMargin
    )
    dfBalance.take(10).foreach { row =>
      println(s"d) Most balanced user: $row")
    }
    sqlContext.sparkContext.stop()
  }
}