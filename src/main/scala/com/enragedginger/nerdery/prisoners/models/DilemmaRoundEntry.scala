package com.enragedginger.nerdery.prisoners.models

import java.util.Date

/**
  * @author Stephen Hopper.
  */
case class DilemmaRoundEntry(startTime: Date,
                             endTime: Date,
                             durationSeconds: Int,
                             prisoner1: String,
                             prisoner2: String,
                             roundIndex: Int,
                             prisoner1Response: String,
                             prisoner2Response: String,
                             prisoner1TimeMs: Int,
                             prisoner2TimeMs: Int)