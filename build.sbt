name := """graphx-scala"""

version := "1.0"

scalaVersion := "2.11.7"

lazy val sparkVersion = "1.6.0"
lazy val spark = "org.apache.spark"

libraryDependencies ++= Seq(
  spark %% "spark-core" % sparkVersion,
  spark %% "spark-sql" % sparkVersion,
  spark %% "spark-streaming" % sparkVersion,
  "com.databricks" %% "spark-csv" % "1.4.0"
)

mainClass in (Compile, run) := Some("com.enragedginger.nerdery.prisoners.Main")

// Change this to another test framework if you prefer
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % "test"
